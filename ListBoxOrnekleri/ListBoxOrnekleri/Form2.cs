﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBoxOrnekleri
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnSagTek_Click(object sender, EventArgs e)
        {
            lbSecilenler.Items.Add(lbKullanicilar.SelectedItem.ToString());
            lbKullanicilar.Items.Remove(lbKullanicilar.SelectedItem.ToString());
        }

        private void btnSolTek_Click(object sender, EventArgs e)
        {
            lbKullanicilar.Items.Add(lbSecilenler.SelectedItem.ToString());
            lbSecilenler.Items.Remove(lbSecilenler.SelectedItem.ToString());
        }

        private void btnSagCok_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lbKullanicilar.SelectedItems.Count; i++)
            {
                lbSecilenler.Items.Add(lbKullanicilar.SelectedItems[i]);
                lbKullanicilar.Items.Remove(lbKullanicilar.SelectedItems[i--]);
            }
        }

        private void btnSolCok_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lbSecilenler.SelectedItems.Count; i++)
            {
                lbKullanicilar.Items.Add(lbSecilenler.SelectedItems[i]);
                lbSecilenler.Items.Remove(lbSecilenler.SelectedItems[i--]);
            }
        }
    }
}
