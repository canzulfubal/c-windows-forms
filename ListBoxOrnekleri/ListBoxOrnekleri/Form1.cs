﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBoxOrnekleri
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            if (txtGirdi.Text.Trim() != "")
            {
                //if (lbListe.Items.Contains(txtGirdi.Text.Trim()) == true)
                if (lbListe.Items.Contains(txtGirdi.Text.Trim().ToLower()))
                {
                    MessageBox.Show("Aynı İsim Mevcut");
                    txtGirdi.Clear();
                    txtGirdi.Focus();
                }
                else
                {
                    lbListe.Items.Add(txtGirdi.Text.Trim().ToLower());
                    txtGirdi.Clear();
                    txtGirdi.Focus();
                }
            }
            else
                MessageBox.Show("İsim Girmelisiniz", "Eksik Bilgi");
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            if (lbListe.SelectedIndex == -1)
            {
                MessageBox.Show("Seçim Yapmadınız");
            }
            else
            {
                if (MessageBox.Show("Emin Misiniz?", lbListe.SelectedItem.ToString() + " Silinsin mi?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    lbListe.Items.RemoveAt(lbListe.SelectedIndex);
                }
            }
        }

        private void btnArayaEkle_Click(object sender, EventArgs e)
        {
            if (txtGirdi.Text != "")
            {
                lbListe.Items.Insert(lbListe.SelectedIndex, txtGirdi.Text.Trim().ToLower());
            }            
        }

        private void btnBul_Click(object sender, EventArgs e)
        {
            if (lbListe.Items.Contains(txtGirdi.Text.ToLower()))
            {
                lbListe.SelectedItem = txtGirdi.Text.ToLower(); //selectedindex ile nasıl çözülür
            }
            else
            {
                MessageBox.Show("Listede Yok!");
            }
        }
    }
}
