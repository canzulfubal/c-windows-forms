﻿namespace ListBoxOrnekleri
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSolCok = new System.Windows.Forms.Button();
            this.btnSolTek = new System.Windows.Forms.Button();
            this.btnSagCok = new System.Windows.Forms.Button();
            this.btnSagTek = new System.Windows.Forms.Button();
            this.lbSecilenler = new System.Windows.Forms.ListBox();
            this.lbKullanicilar = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnSolCok
            // 
            this.btnSolCok.Location = new System.Drawing.Point(217, 230);
            this.btnSolCok.Name = "btnSolCok";
            this.btnSolCok.Size = new System.Drawing.Size(37, 23);
            this.btnSolCok.TabIndex = 4;
            this.btnSolCok.Text = "<<";
            this.btnSolCok.UseVisualStyleBackColor = true;
            this.btnSolCok.Click += new System.EventHandler(this.btnSolCok_Click);
            // 
            // btnSolTek
            // 
            this.btnSolTek.Location = new System.Drawing.Point(217, 189);
            this.btnSolTek.Name = "btnSolTek";
            this.btnSolTek.Size = new System.Drawing.Size(37, 23);
            this.btnSolTek.TabIndex = 5;
            this.btnSolTek.Text = "<";
            this.btnSolTek.UseVisualStyleBackColor = true;
            this.btnSolTek.Click += new System.EventHandler(this.btnSolTek_Click);
            // 
            // btnSagCok
            // 
            this.btnSagCok.Location = new System.Drawing.Point(217, 148);
            this.btnSagCok.Name = "btnSagCok";
            this.btnSagCok.Size = new System.Drawing.Size(37, 23);
            this.btnSagCok.TabIndex = 6;
            this.btnSagCok.Text = ">>";
            this.btnSagCok.UseVisualStyleBackColor = true;
            this.btnSagCok.Click += new System.EventHandler(this.btnSagCok_Click);
            // 
            // btnSagTek
            // 
            this.btnSagTek.Location = new System.Drawing.Point(217, 107);
            this.btnSagTek.Name = "btnSagTek";
            this.btnSagTek.Size = new System.Drawing.Size(37, 23);
            this.btnSagTek.TabIndex = 7;
            this.btnSagTek.Text = ">";
            this.btnSagTek.UseVisualStyleBackColor = true;
            this.btnSagTek.Click += new System.EventHandler(this.btnSagTek_Click);
            // 
            // lbSecilenler
            // 
            this.lbSecilenler.FormattingEnabled = true;
            this.lbSecilenler.Location = new System.Drawing.Point(291, 46);
            this.lbSecilenler.Name = "lbSecilenler";
            this.lbSecilenler.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbSecilenler.Size = new System.Drawing.Size(120, 264);
            this.lbSecilenler.TabIndex = 2;
            // 
            // lbKullanicilar
            // 
            this.lbKullanicilar.FormattingEnabled = true;
            this.lbKullanicilar.Items.AddRange(new object[] {
            "Sümeyye",
            "Elif",
            "Emel",
            "Mahmut",
            "Fatih",
            "Gözde",
            "Gökhan"});
            this.lbKullanicilar.Location = new System.Drawing.Point(61, 46);
            this.lbKullanicilar.Name = "lbKullanicilar";
            this.lbKullanicilar.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbKullanicilar.Size = new System.Drawing.Size(120, 264);
            this.lbKullanicilar.TabIndex = 3;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 377);
            this.Controls.Add(this.btnSolCok);
            this.Controls.Add(this.btnSolTek);
            this.Controls.Add(this.btnSagCok);
            this.Controls.Add(this.btnSagTek);
            this.Controls.Add(this.lbSecilenler);
            this.Controls.Add(this.lbKullanicilar);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSolCok;
        private System.Windows.Forms.Button btnSolTek;
        private System.Windows.Forms.Button btnSagCok;
        private System.Windows.Forms.Button btnSagTek;
        private System.Windows.Forms.ListBox lbSecilenler;
        private System.Windows.Forms.ListBox lbKullanicilar;
    }
}