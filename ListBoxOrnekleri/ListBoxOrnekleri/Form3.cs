﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListBoxOrnekleri
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            cbMarkalar.Items.Add("Apple");
            cbMarkalar.Items.Add("Samsung");
            cbMarkalar.Items.Add("Nokia");
        }

        private void cbMarkalar_SelectedIndexChanged(object sender, EventArgs e)
        {

            lbModeller.Items.Clear();

            if (cbMarkalar.SelectedIndex == 0)
            {
                //lbModeller.Items.Clear(); //yukarı yazmak yeterli
                lbModeller.Items.Add("Iphone 5S");
                lbModeller.Items.Add("Iphone 6");
                lbModeller.Items.Add("Iphone 6S");
            }
            else if (cbMarkalar.SelectedIndex == 1)
            {
                //lbModeller.Items.Clear(); //yukarı yazmak yeterli
                lbModeller.Items.Add("Galaxy S6");
                lbModeller.Items.Add("Galaxy Note4");
                lbModeller.Items.Add("Galaxy Note5");
            }
            else if (cbMarkalar.SelectedIndex == 2)
            {
                //lbModeller.Items.Clear(); //yukarı yazmak yeterli
                lbModeller.Items.Add("Lumia 1120");
                lbModeller.Items.Add("Lumia 1320");
                lbModeller.Items.Add("Lumia 1520");
            }
        }
    }
}
