﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiziOrnekleri
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        int[] Sayilar = { 5, 3, 8, 2, 9, 6, 7 };

        private void Form3_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < Sayilar.Length; i++)
            {
                lbIlkHali.Items.Add(Sayilar[i]);
            }

            Array.Reverse(Sayilar); //Dizi elemanlarının sırasını ters çevirir.
            foreach (int sayi in Sayilar)
            {
                lbSonHali.Items.Add(sayi);
            }

            Array.Sort(Sayilar); //Dizi elemanlarını küçükten büyüğe doğru sıralar.
            foreach (int sayi in Sayilar)
            {
                lbSirali.Items.Add(sayi);
            }
        }
    }
}
