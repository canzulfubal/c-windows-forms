﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiziOrnekleri
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        string[] Ogrenciler = { "Ali", "Oya", "Hasan", "Vedat", "Sevgi", "Ayşe" };

        private void btnYukle_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Ogrenciler.Length; i++)
            {
                lbListe.Items.Add(Ogrenciler[i]);
            }
        }

        private void lbListe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
