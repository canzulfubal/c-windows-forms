﻿namespace DiziOrnekleri
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSirali = new System.Windows.Forms.ListBox();
            this.lbSonHali = new System.Windows.Forms.ListBox();
            this.lbIlkHali = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbSirali
            // 
            this.lbSirali.FormattingEnabled = true;
            this.lbSirali.Location = new System.Drawing.Point(389, 60);
            this.lbSirali.Name = "lbSirali";
            this.lbSirali.Size = new System.Drawing.Size(120, 225);
            this.lbSirali.TabIndex = 5;
            // 
            // lbSonHali
            // 
            this.lbSonHali.FormattingEnabled = true;
            this.lbSonHali.Location = new System.Drawing.Point(243, 60);
            this.lbSonHali.Name = "lbSonHali";
            this.lbSonHali.Size = new System.Drawing.Size(120, 225);
            this.lbSonHali.TabIndex = 4;
            // 
            // lbIlkHali
            // 
            this.lbIlkHali.FormattingEnabled = true;
            this.lbIlkHali.Location = new System.Drawing.Point(97, 60);
            this.lbIlkHali.Name = "lbIlkHali";
            this.lbIlkHali.Size = new System.Drawing.Size(120, 225);
            this.lbIlkHali.TabIndex = 3;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 368);
            this.Controls.Add(this.lbSirali);
            this.Controls.Add(this.lbSonHali);
            this.Controls.Add(this.lbIlkHali);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbSirali;
        private System.Windows.Forms.ListBox lbSonHali;
        private System.Windows.Forms.ListBox lbIlkHali;
    }
}