﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiziOrnekleri
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] Takimlar = { "Galatasaray", "Fenerbahçe", "Beşiktaş", "Trabzonspor", "Bursaspor" };

            foreach (string takim in Takimlar)
            {
                MessageBox.Show(takim);
            }
        }
    }
}
