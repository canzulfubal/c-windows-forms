﻿namespace SayiTahmini
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rbIleri = new System.Windows.Forms.RadioButton();
            this.rbOrta = new System.Windows.Forms.RadioButton();
            this.rbBaslangic = new System.Windows.Forms.RadioButton();
            this.btnGameOver = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDene = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTahmin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pbResim = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbResim)).BeginInit();
            this.SuspendLayout();
            // 
            // rbIleri
            // 
            this.rbIleri.AutoSize = true;
            this.rbIleri.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbIleri.ForeColor = System.Drawing.Color.Blue;
            this.rbIleri.Location = new System.Drawing.Point(431, 145);
            this.rbIleri.Name = "rbIleri";
            this.rbIleri.Size = new System.Drawing.Size(94, 21);
            this.rbIleri.TabIndex = 13;
            this.rbIleri.Text = "İleri Seviye";
            this.rbIleri.UseVisualStyleBackColor = true;
            this.rbIleri.CheckedChanged += new System.EventHandler(this.rbIleri_CheckedChanged);
            // 
            // rbOrta
            // 
            this.rbOrta.AutoSize = true;
            this.rbOrta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbOrta.ForeColor = System.Drawing.Color.Blue;
            this.rbOrta.Location = new System.Drawing.Point(267, 145);
            this.rbOrta.Name = "rbOrta";
            this.rbOrta.Size = new System.Drawing.Size(100, 21);
            this.rbOrta.TabIndex = 14;
            this.rbOrta.Text = "Orta Seviye";
            this.rbOrta.UseVisualStyleBackColor = true;
            this.rbOrta.CheckedChanged += new System.EventHandler(this.rbOrta_CheckedChanged);
            // 
            // rbBaslangic
            // 
            this.rbBaslangic.AutoSize = true;
            this.rbBaslangic.Checked = true;
            this.rbBaslangic.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbBaslangic.ForeColor = System.Drawing.Color.Blue;
            this.rbBaslangic.Location = new System.Drawing.Point(82, 145);
            this.rbBaslangic.Name = "rbBaslangic";
            this.rbBaslangic.Size = new System.Drawing.Size(133, 21);
            this.rbBaslangic.TabIndex = 15;
            this.rbBaslangic.TabStop = true;
            this.rbBaslangic.Text = "Başlangıç Seviye";
            this.rbBaslangic.UseVisualStyleBackColor = true;
            this.rbBaslangic.CheckedChanged += new System.EventHandler(this.rbBaslangic_CheckedChanged);
            // 
            // btnGameOver
            // 
            this.btnGameOver.Location = new System.Drawing.Point(394, 194);
            this.btnGameOver.Name = "btnGameOver";
            this.btnGameOver.Size = new System.Drawing.Size(131, 63);
            this.btnGameOver.TabIndex = 12;
            this.btnGameOver.Text = "Game Over";
            this.btnGameOver.UseVisualStyleBackColor = true;
            this.btnGameOver.Click += new System.EventHandler(this.btnGameOver_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(98, 194);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(131, 63);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDene
            // 
            this.btnDene.Location = new System.Drawing.Point(412, 93);
            this.btnDene.Name = "btnDene";
            this.btnDene.Size = new System.Drawing.Size(75, 23);
            this.btnDene.TabIndex = 10;
            this.btnDene.Text = "DENE";
            this.btnDene.UseVisualStyleBackColor = true;
            this.btnDene.Click += new System.EventHandler(this.btnDene_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(144, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tahmininiz:";
            // 
            // txtTahmin
            // 
            this.txtTahmin.Location = new System.Drawing.Point(254, 95);
            this.txtTahmin.Name = "txtTahmin";
            this.txtTahmin.Size = new System.Drawing.Size(100, 20);
            this.txtTahmin.TabIndex = 8;
            this.txtTahmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(31, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(572, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "1 ile 50 Arasında Bir Sayı Tuttum. Bakalım Kaç Defada Bulabileceksin?";
            // 
            // pbResim
            // 
            this.pbResim.Image = ((System.Drawing.Image)(resources.GetObject("pbResim.Image")));
            this.pbResim.Location = new System.Drawing.Point(173, 315);
            this.pbResim.Name = "pbResim";
            this.pbResim.Size = new System.Drawing.Size(265, 206);
            this.pbResim.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbResim.TabIndex = 16;
            this.pbResim.TabStop = false;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnDene;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(632, 284);
            this.Controls.Add(this.pbResim);
            this.Controls.Add(this.rbIleri);
            this.Controls.Add(this.rbOrta);
            this.Controls.Add(this.rbBaslangic);
            this.Controls.Add(this.btnGameOver);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDene);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTahmin);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbResim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbIleri;
        private System.Windows.Forms.RadioButton rbOrta;
        private System.Windows.Forms.RadioButton rbBaslangic;
        private System.Windows.Forms.Button btnGameOver;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDene;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTahmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbResim;
    }
}

