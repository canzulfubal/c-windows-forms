﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SayiTahmini
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int TutulanSayi = 0; //oto bulacak
        int Tahmin = 0;
        int Sayac = 0;
        int KacHak = 10;
        Random rnd = new Random();

        private void btnDene_Click(object sender, EventArgs e)
        {
            if (txtTahmin.Text.Trim() == "")
            {
                MessageBox.Show("Bir Sayı Girmelisiniz!");
            }
            else
            {
                Sayac++;
                Tahmin = Convert.ToInt32(txtTahmin.Text);
                if (Tahmin > TutulanSayi)
                {
                    MessageBox.Show("Tahmininiz Büyük");
                    txtTahmin.Clear();
                    txtTahmin.Focus();
                }
                else if (Tahmin < TutulanSayi)
                {
                    MessageBox.Show("Tahmininiz Küçük");
                    txtTahmin.Clear();
                    txtTahmin.Focus();
                }
                else
                {
                    this.Height = 600;
                    MessageBox.Show("Tebrikler " + Sayac + ".de Bildiniz");
                    this.Height = 325;
                    Sayac = 0;
                    TutulanSayi = rnd.Next(1, 51);
                    txtTahmin.Clear();
                    txtTahmin.Focus();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TutulanSayi = rnd.Next(1, 51);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGameOver_Click(object sender, EventArgs e)
        {
            Sayac = 0;
            TutulanSayi = rnd.Next(1, 51);
            txtTahmin.Clear();
            txtTahmin.Focus();
            rbBaslangic.Checked = true;
        }

        private void rbOrta_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOrta.Checked == true)
            {
                TutulanSayi = rnd.Next(1, 151);
                label1.Text = "1 ile 150 Arasında Bir Sayı Tuttum. Bakalım Kaç Defada Bulabileceksin?";
            }
        }

        private void rbIleri_CheckedChanged(object sender, EventArgs e)
        {
            if (rbIleri.Checked == true)
            {
                TutulanSayi = rnd.Next(1, 251);
                label1.Text = "1 ile 250 Arasında Bir Sayı Tuttum. Bakalım Kaç Defada Bulabileceksin?";
            }
        }

        private void rbBaslangic_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBaslangic.Checked == true)
            {
                TutulanSayi = rnd.Next(1, 51);
                label1.Text = "1 ile 50 Arasında Bir Sayı Tuttum. Bakalım Kaç Defada Bulabileceksin?";
            }
        }
    }
}
