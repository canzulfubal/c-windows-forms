﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CevreAlanHesabi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Alan ve Çevre Hesaplama";
        }

        const float pi = 3.14f;

        private void btnCevre_Click(object sender, EventArgs e)
        {
            if (txtYaricap.Text.Trim()!="")
            {
                float r = Convert.ToSingle(txtYaricap.Text);
                MessageBox.Show(Convert.ToString(2 * pi * r));
            }
            else
            {
                MessageBox.Show("Eksik ya da Yanlış Veri Girdiniz!");
            }
        }

        private void btnAlan_Click(object sender, EventArgs e)
        {
            if (txtYaricap.Text.Trim() != "")
            {
                float r = Convert.ToSingle(txtYaricap.Text);
                MessageBox.Show(Convert.ToString(pi * r * r));
            }
            else
            {
                MessageBox.Show("Eksik ya da Yanlış Veri Girdiniz!");
            }
        }
    }
}
