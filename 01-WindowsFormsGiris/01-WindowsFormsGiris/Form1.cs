﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01_WindowsFormsGiris
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hoşgeldin " + txtMesaj.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "İlk Form Örneği";
        }

        private void txtMesaj_TextChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(txtMesaj.Text);
        }
    }
}
