﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donguler
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSayilar_Click(object sender, EventArgs e)
        {
            lbSayilar.Items.Clear();
            int j = 0;
            for (int i = 0; i <= 10; i++)
            {
                lbSayilar.Items.Add(i);
                j = j + i;
            }
            txtToplamSayilar.Text = Convert.ToString(j);
        }

        private void btnCiftSayilar_Click(object sender, EventArgs e)
        {
            lbCift.Items.Clear();
            int j = 0;
            for (int i = 0; i <= 10; i=i+2)
            {
                lbCift.Items.Add(i);
                j = j + i;
            }
            txtCiftToplam.Text = Convert.ToString(j);
        }

        private void btnTekSayilar_Click(object sender, EventArgs e)
        {
            lbTek.Items.Clear();
            int j = 0;
            for (int i = 1; i <= 10; i = i + 2)
            {
                lbTek.Items.Add(i);
                j = j + i;
            }
            txtTekToplam.Text = Convert.ToString(j);
        }
    }
}
