﻿namespace Donguler
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSayilar = new System.Windows.Forms.Button();
            this.lbSayilar = new System.Windows.Forms.ListBox();
            this.txtToplamSayilar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCiftSayilar = new System.Windows.Forms.Button();
            this.lbCift = new System.Windows.Forms.ListBox();
            this.txtCiftToplam = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTekSayilar = new System.Windows.Forms.Button();
            this.lbTek = new System.Windows.Forms.ListBox();
            this.txtTekToplam = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSayilar
            // 
            this.btnSayilar.Location = new System.Drawing.Point(95, 49);
            this.btnSayilar.Name = "btnSayilar";
            this.btnSayilar.Size = new System.Drawing.Size(75, 23);
            this.btnSayilar.TabIndex = 0;
            this.btnSayilar.Text = "Sayılar";
            this.btnSayilar.UseVisualStyleBackColor = true;
            this.btnSayilar.Click += new System.EventHandler(this.btnSayilar_Click);
            // 
            // lbSayilar
            // 
            this.lbSayilar.FormattingEnabled = true;
            this.lbSayilar.Location = new System.Drawing.Point(77, 78);
            this.lbSayilar.Name = "lbSayilar";
            this.lbSayilar.Size = new System.Drawing.Size(112, 134);
            this.lbSayilar.TabIndex = 1;
            // 
            // txtToplamSayilar
            // 
            this.txtToplamSayilar.Location = new System.Drawing.Point(123, 218);
            this.txtToplamSayilar.Name = "txtToplamSayilar";
            this.txtToplamSayilar.Size = new System.Drawing.Size(47, 20);
            this.txtToplamSayilar.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Toplam";
            // 
            // btnCiftSayilar
            // 
            this.btnCiftSayilar.Location = new System.Drawing.Point(262, 49);
            this.btnCiftSayilar.Name = "btnCiftSayilar";
            this.btnCiftSayilar.Size = new System.Drawing.Size(75, 23);
            this.btnCiftSayilar.TabIndex = 0;
            this.btnCiftSayilar.Text = "Çift Sayılar";
            this.btnCiftSayilar.UseVisualStyleBackColor = true;
            this.btnCiftSayilar.Click += new System.EventHandler(this.btnCiftSayilar_Click);
            // 
            // lbCift
            // 
            this.lbCift.FormattingEnabled = true;
            this.lbCift.Location = new System.Drawing.Point(244, 78);
            this.lbCift.Name = "lbCift";
            this.lbCift.Size = new System.Drawing.Size(112, 134);
            this.lbCift.TabIndex = 1;
            // 
            // txtCiftToplam
            // 
            this.txtCiftToplam.Location = new System.Drawing.Point(290, 218);
            this.txtCiftToplam.Name = "txtCiftToplam";
            this.txtCiftToplam.Size = new System.Drawing.Size(47, 20);
            this.txtCiftToplam.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(241, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Toplam";
            // 
            // btnTekSayilar
            // 
            this.btnTekSayilar.Location = new System.Drawing.Point(415, 49);
            this.btnTekSayilar.Name = "btnTekSayilar";
            this.btnTekSayilar.Size = new System.Drawing.Size(75, 23);
            this.btnTekSayilar.TabIndex = 0;
            this.btnTekSayilar.Text = "Tek Sayılar";
            this.btnTekSayilar.UseVisualStyleBackColor = true;
            this.btnTekSayilar.Click += new System.EventHandler(this.btnTekSayilar_Click);
            // 
            // lbTek
            // 
            this.lbTek.FormattingEnabled = true;
            this.lbTek.Location = new System.Drawing.Point(397, 78);
            this.lbTek.Name = "lbTek";
            this.lbTek.Size = new System.Drawing.Size(112, 134);
            this.lbTek.TabIndex = 1;
            // 
            // txtTekToplam
            // 
            this.txtTekToplam.Location = new System.Drawing.Point(443, 218);
            this.txtTekToplam.Name = "txtTekToplam";
            this.txtTekToplam.Size = new System.Drawing.Size(47, 20);
            this.txtTekToplam.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(394, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Toplam";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(584, 293);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTekToplam);
            this.Controls.Add(this.txtCiftToplam);
            this.Controls.Add(this.txtToplamSayilar);
            this.Controls.Add(this.lbTek);
            this.Controls.Add(this.btnTekSayilar);
            this.Controls.Add(this.lbCift);
            this.Controls.Add(this.btnCiftSayilar);
            this.Controls.Add(this.lbSayilar);
            this.Controls.Add(this.btnSayilar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSayilar;
        private System.Windows.Forms.ListBox lbSayilar;
        private System.Windows.Forms.TextBox txtToplamSayilar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCiftSayilar;
        private System.Windows.Forms.ListBox lbCift;
        private System.Windows.Forms.TextBox txtCiftToplam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTekSayilar;
        private System.Windows.Forms.ListBox lbTek;
        private System.Windows.Forms.TextBox txtTekToplam;
        private System.Windows.Forms.Label label3;
    }
}

