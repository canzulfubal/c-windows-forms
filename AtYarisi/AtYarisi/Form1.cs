﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtYarisi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random rnd = new Random();
        int at1 = 0;
        int at2 = 0;
        int at3 = 0;
        int at4 = 0;

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            pb1.Left = 30;
            pb2.Left = 30;
            pb3.Left = 30;
            pb4.Left = 30;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            at1 = rnd.Next(1, 8);
            at2 = rnd.Next(1, 8);
            at3 = rnd.Next(1, 8);
            at4 = rnd.Next(1, 8);
            pb1.Left += at1;
            pb2.Left += at2;
            pb3.Left += at3;
            pb4.Left += at4;

            if (pb1.Left >= (this.Width - pb1.Width))
            {
                timer1.Enabled = false;
                MessageBox.Show("1. At Kazandı");
            }
             if (pb2.Left >= (this.Width - pb2.Width))
            {
                timer1.Enabled = false;
                MessageBox.Show("2. At Kazandı");
            }
             if(pb3.Left >= (this.Width - pb3.Width))
            {
                timer1.Enabled = false;
                MessageBox.Show("3. At Kazandı");
            }
             if(pb4.Left >= (this.Width - pb4.Width))
            {
                timer1.Enabled = false;
                MessageBox.Show("4. At Kazandı");
            }
        }
    }
}
