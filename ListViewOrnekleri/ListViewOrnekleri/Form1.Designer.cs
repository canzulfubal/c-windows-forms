﻿namespace ListViewOrnekleri
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvDetaylar = new System.Windows.Forms.ListView();
            this.islemci = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hafizaram = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hafizasabit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ekran = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.kamera = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.fiyat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.lbModeller = new System.Windows.Forms.ListBox();
            this.cbMarkalar = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lvDetaylar
            // 
            this.lvDetaylar.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.islemci,
            this.hafizaram,
            this.hafizasabit,
            this.ekran,
            this.kamera,
            this.fiyat});
            this.lvDetaylar.FullRowSelect = true;
            this.lvDetaylar.GridLines = true;
            this.lvDetaylar.Location = new System.Drawing.Point(223, 78);
            this.lvDetaylar.Name = "lvDetaylar";
            this.lvDetaylar.Size = new System.Drawing.Size(549, 342);
            this.lvDetaylar.TabIndex = 10;
            this.lvDetaylar.UseCompatibleStateImageBehavior = false;
            this.lvDetaylar.View = System.Windows.Forms.View.Details;
            // 
            // islemci
            // 
            this.islemci.Text = "İşlemci";
            this.islemci.Width = 90;
            // 
            // hafizaram
            // 
            this.hafizaram.Text = "Hafıza (RAM)";
            this.hafizaram.Width = 90;
            // 
            // hafizasabit
            // 
            this.hafizasabit.Text = "Hafıza (Sabit)";
            this.hafizasabit.Width = 90;
            // 
            // ekran
            // 
            this.ekran.Text = "Ekran";
            this.ekran.Width = 90;
            // 
            // kamera
            // 
            this.kamera.Text = "Kamera";
            this.kamera.Width = 90;
            // 
            // fiyat
            // 
            this.fiyat.Text = "Fiyat";
            this.fiyat.Width = 90;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Marka Seçiniz";
            // 
            // lbModeller
            // 
            this.lbModeller.FormattingEnabled = true;
            this.lbModeller.Location = new System.Drawing.Point(12, 78);
            this.lbModeller.Name = "lbModeller";
            this.lbModeller.Size = new System.Drawing.Size(205, 342);
            this.lbModeller.TabIndex = 8;
            this.lbModeller.SelectedIndexChanged += new System.EventHandler(this.lbModeller_SelectedIndexChanged);
            // 
            // cbMarkalar
            // 
            this.cbMarkalar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarkalar.FormattingEnabled = true;
            this.cbMarkalar.Location = new System.Drawing.Point(12, 43);
            this.cbMarkalar.Name = "cbMarkalar";
            this.cbMarkalar.Size = new System.Drawing.Size(205, 21);
            this.cbMarkalar.TabIndex = 7;
            this.cbMarkalar.SelectedIndexChanged += new System.EventHandler(this.cbMarkalar_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 432);
            this.Controls.Add(this.lvDetaylar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbModeller);
            this.Controls.Add(this.cbMarkalar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvDetaylar;
        private System.Windows.Forms.ColumnHeader islemci;
        private System.Windows.Forms.ColumnHeader hafizaram;
        private System.Windows.Forms.ColumnHeader hafizasabit;
        private System.Windows.Forms.ColumnHeader ekran;
        private System.Windows.Forms.ColumnHeader kamera;
        private System.Windows.Forms.ColumnHeader fiyat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbModeller;
        private System.Windows.Forms.ComboBox cbMarkalar;
    }
}

