﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListViewOrnekleri
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            cbMarkalar.Items.Add("Apple");
            cbMarkalar.Items.Add("Samsung");
            cbMarkalar.Items.Add("Nokia");
        }

        private void cbMarkalar_SelectedIndexChanged(object sender, EventArgs e)
        {

            lvDetaylar.Items.Clear();
            if (cbMarkalar.SelectedIndex == 0)
            {
                lvDetaylar.Items.Add("Iphone 5S", 0);
                lvDetaylar.Items[0].SubItems.Add("1.2 Ghz");
                lvDetaylar.Items[0].SubItems.Add("1 GB");
                lvDetaylar.Items[0].SubItems.Add("16 GB");
                lvDetaylar.Items[0].SubItems.Add("4 \"");
                lvDetaylar.Items[0].SubItems.Add("8 Mp");
                lvDetaylar.Items[0].SubItems.Add("1499");

                lvDetaylar.Items.Add("Iphone 6", 1);
                lvDetaylar.Items[1].SubItems.Add("1.4 Ghz");
                lvDetaylar.Items[1].SubItems.Add("2 GB");
                lvDetaylar.Items[1].SubItems.Add("32 GB");
                lvDetaylar.Items[1].SubItems.Add("5 \"");
                lvDetaylar.Items[1].SubItems.Add("13 Mp");
                lvDetaylar.Items[1].SubItems.Add("2599");

                lvDetaylar.Items.Add("Iphone 6S", 2);
                lvDetaylar.Items[2].SubItems.Add("1.4 Ghz");
                lvDetaylar.Items[2].SubItems.Add("2 GB");
                lvDetaylar.Items[2].SubItems.Add("64 GB");
                lvDetaylar.Items[2].SubItems.Add("5 \"");
                lvDetaylar.Items[2].SubItems.Add("13 Mp");
                lvDetaylar.Items[2].SubItems.Add("2999");

            }
            else if (cbMarkalar.SelectedIndex == 1)
            {
                lvDetaylar.Items.Add("Galaxy S6", 0);
                lvDetaylar.Items[0].SubItems.Add("1,6 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("32 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("8 Mp");
                lvDetaylar.Items[0].SubItems.Add("2199");

                lvDetaylar.Items.Add("Galaxy Note4", 1);
                lvDetaylar.Items[1].SubItems.Add("1,4 Ghz");
                lvDetaylar.Items[1].SubItems.Add("2 GB");
                lvDetaylar.Items[1].SubItems.Add("64 GB");
                lvDetaylar.Items[1].SubItems.Add("5,5 \"");
                lvDetaylar.Items[1].SubItems.Add("13 Mp");
                lvDetaylar.Items[1].SubItems.Add("2299");

                lvDetaylar.Items.Add("Galaxy Note5", 2);
                lvDetaylar.Items[2].SubItems.Add("2,0 Ghz");
                lvDetaylar.Items[2].SubItems.Add("2 GB");
                lvDetaylar.Items[2].SubItems.Add("64 GB");
                lvDetaylar.Items[2].SubItems.Add("6 \"");
                lvDetaylar.Items[2].SubItems.Add("16 Mp");
                lvDetaylar.Items[2].SubItems.Add("2699");
            }
            else if (cbMarkalar.SelectedIndex == 2)
            {
                lvDetaylar.Items.Add("Lumia 1120", 0);
                lvDetaylar.Items[0].SubItems.Add("1.8 Ghz");
                lvDetaylar.Items[0].SubItems.Add("1 GB");
                lvDetaylar.Items[0].SubItems.Add("16 GB");
                lvDetaylar.Items[0].SubItems.Add("4,3 \"");
                lvDetaylar.Items[0].SubItems.Add("5 Mp");
                lvDetaylar.Items[0].SubItems.Add("1199");

                lvDetaylar.Items.Add("Lumia 1320", 1);
                lvDetaylar.Items[1].SubItems.Add("1.4 Ghz");
                lvDetaylar.Items[1].SubItems.Add("1 GB");
                lvDetaylar.Items[1].SubItems.Add("16 GB");
                lvDetaylar.Items[1].SubItems.Add("5 \"");
                lvDetaylar.Items[1].SubItems.Add("8 Mp");
                lvDetaylar.Items[1].SubItems.Add("1399");

                lvDetaylar.Items.Add("Lumia 1520", 2);
                lvDetaylar.Items[2].SubItems.Add("1.4 Ghz");
                lvDetaylar.Items[2].SubItems.Add("2 GB");
                lvDetaylar.Items[2].SubItems.Add("64 GB");
                lvDetaylar.Items[2].SubItems.Add("5 \"");
                lvDetaylar.Items[2].SubItems.Add("13 Mp");
                lvDetaylar.Items[2].SubItems.Add("1799");
            }
        }

        private void lvDetaylar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvDetaylar.SelectedItems.Count > 0)
            {
                MessageBox.Show(lvDetaylar.SelectedItems[0].SubItems[0].Text + ", fiyatı = " +
                    lvDetaylar.SelectedItems[0].SubItems[6].Text);
            }
        }

        private void mitmLargeIcon_Click(object sender, EventArgs e)
        {
            lvDetaylar.View = View.LargeIcon;
        }

        private void mitmSmallIcon_Click(object sender, EventArgs e)
        {
            lvDetaylar.View = View.SmallIcon;
        }

        private void mitmDetails_Click(object sender, EventArgs e)
        {
            lvDetaylar.View = View.Details;
        }

        private void mitmList_Click(object sender, EventArgs e)
        {
            lvDetaylar.View = View.List;
        }

        private void mitmTile_Click(object sender, EventArgs e)
        {
            lvDetaylar.View = View.Tile;
        }
    }
}
