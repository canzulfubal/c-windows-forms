﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListViewOrnekleri
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbMarkalar.Items.Add("Apple");
            cbMarkalar.Items.Add("Samsung");
            cbMarkalar.Items.Add("Nokia");
        }

        private void cbMarkalar_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbModeller.Items.Clear();
            if (cbMarkalar.SelectedIndex == 0)
            {
                lbModeller.Items.Add("Iphone 5S");
                lbModeller.Items.Add("Iphone 6");
                lbModeller.Items.Add("Iphone 6S");
            }
            else if (cbMarkalar.SelectedIndex == 1)
            {
                lbModeller.Items.Add("Galaxy S6");
                lbModeller.Items.Add("Galaxy Note4");
                lbModeller.Items.Add("Galaxy Note5");
            }
            else if (cbMarkalar.SelectedIndex == 2)
            {
                lbModeller.Items.Add("Lumia 1120");
                lbModeller.Items.Add("Lumia 1320");
                lbModeller.Items.Add("Lumia 1520");
            }
        }

        private void lbModeller_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvDetaylar.Items.Clear();
            if (lbModeller.SelectedItem.ToString() == "Iphone 5S")
            {
                lvDetaylar.Items.Add("1.2 Ghz");
                lvDetaylar.Items[0].SubItems.Add("1 GB");
                lvDetaylar.Items[0].SubItems.Add("16 GB");
                lvDetaylar.Items[0].SubItems.Add("4 \"");
                lvDetaylar.Items[0].SubItems.Add("8 Mp");
                lvDetaylar.Items[0].SubItems.Add("1499");
            }
            else if (lbModeller.SelectedItem.ToString() == "Iphone 6")
            {
                lvDetaylar.Items.Add("1.4 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("32 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("13 Mp");
                lvDetaylar.Items[0].SubItems.Add("2599");
            }
            else if (lbModeller.SelectedItem.ToString() == "Iphone 6S")
            {
                lvDetaylar.Items.Add("1.4 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("64 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("13 Mp");
                lvDetaylar.Items[0].SubItems.Add("2999");
            }
            else if (lbModeller.SelectedItem.ToString() == "Galaxy S6")
            {
                lvDetaylar.Items.Add("1,6 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("32 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("8 Mp");
                lvDetaylar.Items[0].SubItems.Add("2199");
            }
            else if (lbModeller.SelectedItem.ToString() == "Galaxy Note4")
            {
                lvDetaylar.Items.Add("1,4 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("64 GB");
                lvDetaylar.Items[0].SubItems.Add("5,5 \"");
                lvDetaylar.Items[0].SubItems.Add("13 Mp");
                lvDetaylar.Items[0].SubItems.Add("2299");
            }
            else if (lbModeller.SelectedItem.ToString() == "Galaxy Note5")
            {
                lvDetaylar.Items.Add("2,0 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("64 GB");
                lvDetaylar.Items[0].SubItems.Add("6 \"");
                lvDetaylar.Items[0].SubItems.Add("16 Mp");
                lvDetaylar.Items[0].SubItems.Add("2699");
            }
            else if (lbModeller.SelectedItem.ToString() == "Lumia 1120")
            {
                lvDetaylar.Items.Add("1.8 Ghz");
                lvDetaylar.Items[0].SubItems.Add("1 GB");
                lvDetaylar.Items[0].SubItems.Add("16 GB");
                lvDetaylar.Items[0].SubItems.Add("4,3 \"");
                lvDetaylar.Items[0].SubItems.Add("5 Mp");
                lvDetaylar.Items[0].SubItems.Add("1199");
            }
            else if (lbModeller.SelectedItem.ToString() == "Lumia 1320")
            {
                lvDetaylar.Items.Add("1.4 Ghz");
                lvDetaylar.Items[0].SubItems.Add("1 GB");
                lvDetaylar.Items[0].SubItems.Add("16 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("8 Mp");
                lvDetaylar.Items[0].SubItems.Add("1399");
            }
            else if (lbModeller.SelectedItem.ToString() == "Lumia 1520")
            {
                lvDetaylar.Items.Add("1.4 Ghz");
                lvDetaylar.Items[0].SubItems.Add("2 GB");
                lvDetaylar.Items[0].SubItems.Add("64 GB");
                lvDetaylar.Items[0].SubItems.Add("5 \"");
                lvDetaylar.Items[0].SubItems.Add("13 Mp");
                lvDetaylar.Items[0].SubItems.Add("1799");
            }
        }
    }
}
