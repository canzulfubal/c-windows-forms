﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LuckySevenOyunu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random rnd = new Random();
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "START")
            {
                timer1.Enabled = true;
                btnStart.Text = "STOP";
            }
            else
            {
                timer1.Enabled = false;
                btnStart.Text = "START";
                if (s1 == s2 && s1 == s3 && s2 == s3)
                {
                    this.Height = 475;
                    MessageBox.Show("Tebrikler 3 Tane Bildiniz");
                }
                else if (s1 == s2 || s1 == s3 || s2 == s3)
                    MessageBox.Show("Tebrikler 2 Tane Tutturdunuz");
                else
                    MessageBox.Show("Malesef Hiç Tutturamadınız");
            }
            this.Height = 265;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            s1 = rnd.Next(1, 8);
            s2 = rnd.Next(1, 8);
            s3 = rnd.Next(1, 8);
            lbl1.Text = Convert.ToString(s1);
            lbl2.Text = Convert.ToString(s2);
            lbl3.Text = Convert.ToString(s3);
        }
    }
}
