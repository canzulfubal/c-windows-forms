﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IsBasvuruFormu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBasvur_Click(object sender, EventArgs e)
        {
            if (txtAd.Text != "" && txtSoyad.Text != "" && txtTelefon.Text != "" && txtTC.Text != "" && txtAdres.Text != "" && txtNotlar.Text != "" && cbEgitim.Text != "")
            {
                int Yas = DateTime.Now.Year - dtpDTarihi.Value.Year;

                if (Yas < 18 || Yas > 65)
                    MessageBox.Show("Üzgünüz Yaşınız Uygun Değil");

                else if (cbEgitim.SelectedItem == "Lise" || cbEgitim.SelectedItem == "Doktora")
                    MessageBox.Show("Eğitim Seviyeniz Bu İş için Uygun Değil");
                else if (cbxIngilizce.Checked == false)
                    MessageBox.Show("Yabancı Dil Seviyeniz Bu İş için Uygun Değil");
                else if (cbxRusca.Checked == false && cbxArapca.Checked == false)
                    MessageBox.Show("Yabancı Dil Seviyeniz Bu İş için Uygun Değil");

                else if (cbxYazilim.Checked == false || cbxOfis.Checked == false)
                    MessageBox.Show("Yazılım ve Ofis Seviyeniz Bu İş için Uygun Değil");
                else if (cbxVeriTabani.Checked == false && cbxWebTasarim.Checked == false)
                    MessageBox.Show("Veri Tabanı ve Web Tasarım Seviyeniz Bu İş için Uygun Değil");

                else if (rbErkek.Checked == true && rbYapilmadi.Checked == true)
                    MessageBox.Show("Askerlik Durumunuz Uygun Değil");
                else
                    MessageBox.Show("Tebrikler, Sn." + txtAd.Text + " " + txtSoyad.Text + " İşe Alındınız...");
            }
            else
                MessageBox.Show("Lütfen Eksik Yerleri Tamamlayınız.");
        }

        private void rbErkek_CheckedChanged(object sender, EventArgs e)
        {
            gbAskerlik.Enabled = true;
        }

        private void rbKadin_CheckedChanged(object sender, EventArgs e)
        {
            gbAskerlik.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbEgitim.SelectedIndex = 0;
        }
    }
}
