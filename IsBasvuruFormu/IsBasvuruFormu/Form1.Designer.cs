﻿namespace IsBasvuruFormu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtTelefon = new System.Windows.Forms.MaskedTextBox();
            this.lblBaslik = new System.Windows.Forms.Label();
            this.btnBasvur = new System.Windows.Forms.Button();
            this.cbEgitim = new System.Windows.Forms.ComboBox();
            this.gbAskerlik = new System.Windows.Forms.GroupBox();
            this.rbYapilmadi = new System.Windows.Forms.RadioButton();
            this.rbTecilli = new System.Windows.Forms.RadioButton();
            this.rbYapildi = new System.Windows.Forms.RadioButton();
            this.gbCinsiyet = new System.Windows.Forms.GroupBox();
            this.rbKadin = new System.Windows.Forms.RadioButton();
            this.rbErkek = new System.Windows.Forms.RadioButton();
            this.gbBilgisayarBilgisi = new System.Windows.Forms.GroupBox();
            this.cbxWebTasarim = new System.Windows.Forms.CheckBox();
            this.cbxVeriTabani = new System.Windows.Forms.CheckBox();
            this.cbxOfis = new System.Windows.Forms.CheckBox();
            this.cbxSistem = new System.Windows.Forms.CheckBox();
            this.cbxYazilim = new System.Windows.Forms.CheckBox();
            this.gbYabanciDil = new System.Windows.Forms.GroupBox();
            this.cbxArapca = new System.Windows.Forms.CheckBox();
            this.cbxRusca = new System.Windows.Forms.CheckBox();
            this.cbxAlmanca = new System.Windows.Forms.CheckBox();
            this.cbxFransizca = new System.Windows.Forms.CheckBox();
            this.cbxIngilizce = new System.Windows.Forms.CheckBox();
            this.dtpDTarihi = new System.Windows.Forms.DateTimePicker();
            this.txtNotlar = new System.Windows.Forms.TextBox();
            this.txtAdres = new System.Windows.Forms.TextBox();
            this.txtTC = new System.Windows.Forms.TextBox();
            this.txtSoyad = new System.Windows.Forms.TextBox();
            this.txtAd = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblNotlar = new System.Windows.Forms.Label();
            this.lblEgitim = new System.Windows.Forms.Label();
            this.lblDTarih = new System.Windows.Forms.Label();
            this.lblTC = new System.Windows.Forms.Label();
            this.lblAdres = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblSoyad = new System.Windows.Forms.Label();
            this.lblAd = new System.Windows.Forms.Label();
            this.gbAskerlik.SuspendLayout();
            this.gbCinsiyet.SuspendLayout();
            this.gbBilgisayarBilgisi.SuspendLayout();
            this.gbYabanciDil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(140, 178);
            this.txtTelefon.Mask = "(999) 000-00-00";
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(215, 20);
            this.txtTelefon.TabIndex = 36;
            // 
            // lblBaslik
            // 
            this.lblBaslik.AutoSize = true;
            this.lblBaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblBaslik.ForeColor = System.Drawing.Color.Red;
            this.lblBaslik.Location = new System.Drawing.Point(263, 26);
            this.lblBaslik.Name = "lblBaslik";
            this.lblBaslik.Size = new System.Drawing.Size(224, 24);
            this.lblBaslik.TabIndex = 35;
            this.lblBaslik.Text = "İŞE BAŞVURU FORMU";
            // 
            // btnBasvur
            // 
            this.btnBasvur.Location = new System.Drawing.Point(352, 585);
            this.btnBasvur.Name = "btnBasvur";
            this.btnBasvur.Size = new System.Drawing.Size(122, 23);
            this.btnBasvur.TabIndex = 34;
            this.btnBasvur.Text = "İŞE BAŞVUR";
            this.btnBasvur.UseVisualStyleBackColor = true;
            this.btnBasvur.Click += new System.EventHandler(this.btnBasvur_Click);
            // 
            // cbEgitim
            // 
            this.cbEgitim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEgitim.FormattingEnabled = true;
            this.cbEgitim.Items.AddRange(new object[] {
            "Lise",
            "Ön Lisans",
            "Lisans",
            "Yüksek Lisans",
            "Doktora"});
            this.cbEgitim.Location = new System.Drawing.Point(138, 429);
            this.cbEgitim.Name = "cbEgitim";
            this.cbEgitim.Size = new System.Drawing.Size(217, 21);
            this.cbEgitim.TabIndex = 33;
            // 
            // gbAskerlik
            // 
            this.gbAskerlik.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbAskerlik.Controls.Add(this.rbYapilmadi);
            this.gbAskerlik.Controls.Add(this.rbTecilli);
            this.gbAskerlik.Controls.Add(this.rbYapildi);
            this.gbAskerlik.Enabled = false;
            this.gbAskerlik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbAskerlik.Location = new System.Drawing.Point(521, 365);
            this.gbAskerlik.Name = "gbAskerlik";
            this.gbAskerlik.Size = new System.Drawing.Size(201, 80);
            this.gbAskerlik.TabIndex = 32;
            this.gbAskerlik.TabStop = false;
            this.gbAskerlik.Text = "Askerlik";
            // 
            // rbYapilmadi
            // 
            this.rbYapilmadi.AutoSize = true;
            this.rbYapilmadi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbYapilmadi.Location = new System.Drawing.Point(25, 49);
            this.rbYapilmadi.Name = "rbYapilmadi";
            this.rbYapilmadi.Size = new System.Drawing.Size(87, 20);
            this.rbYapilmadi.TabIndex = 2;
            this.rbYapilmadi.TabStop = true;
            this.rbYapilmadi.Text = "Yapılmadı";
            this.rbYapilmadi.UseVisualStyleBackColor = true;
            // 
            // rbTecilli
            // 
            this.rbTecilli.AutoSize = true;
            this.rbTecilli.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbTecilli.Location = new System.Drawing.Point(121, 39);
            this.rbTecilli.Name = "rbTecilli";
            this.rbTecilli.Size = new System.Drawing.Size(62, 20);
            this.rbTecilli.TabIndex = 1;
            this.rbTecilli.TabStop = true;
            this.rbTecilli.Text = "Tecilli";
            this.rbTecilli.UseVisualStyleBackColor = true;
            // 
            // rbYapildi
            // 
            this.rbYapildi.AutoSize = true;
            this.rbYapildi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbYapildi.Location = new System.Drawing.Point(25, 23);
            this.rbYapildi.Name = "rbYapildi";
            this.rbYapildi.Size = new System.Drawing.Size(68, 20);
            this.rbYapildi.TabIndex = 0;
            this.rbYapildi.TabStop = true;
            this.rbYapildi.Text = "Yapıldı";
            this.rbYapildi.UseVisualStyleBackColor = true;
            // 
            // gbCinsiyet
            // 
            this.gbCinsiyet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbCinsiyet.Controls.Add(this.rbKadin);
            this.gbCinsiyet.Controls.Add(this.rbErkek);
            this.gbCinsiyet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbCinsiyet.Location = new System.Drawing.Point(390, 365);
            this.gbCinsiyet.Name = "gbCinsiyet";
            this.gbCinsiyet.Size = new System.Drawing.Size(124, 80);
            this.gbCinsiyet.TabIndex = 31;
            this.gbCinsiyet.TabStop = false;
            this.gbCinsiyet.Text = "Cinsiyet";
            // 
            // rbKadin
            // 
            this.rbKadin.AutoSize = true;
            this.rbKadin.Checked = true;
            this.rbKadin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbKadin.Location = new System.Drawing.Point(28, 23);
            this.rbKadin.Name = "rbKadin";
            this.rbKadin.Size = new System.Drawing.Size(60, 20);
            this.rbKadin.TabIndex = 4;
            this.rbKadin.TabStop = true;
            this.rbKadin.Text = "Kadın";
            this.rbKadin.UseVisualStyleBackColor = true;
            this.rbKadin.CheckedChanged += new System.EventHandler(this.rbKadin_CheckedChanged);
            // 
            // rbErkek
            // 
            this.rbErkek.AutoSize = true;
            this.rbErkek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbErkek.Location = new System.Drawing.Point(28, 49);
            this.rbErkek.Name = "rbErkek";
            this.rbErkek.Size = new System.Drawing.Size(61, 20);
            this.rbErkek.TabIndex = 5;
            this.rbErkek.Text = "Erkek";
            this.rbErkek.UseVisualStyleBackColor = true;
            this.rbErkek.CheckedChanged += new System.EventHandler(this.rbErkek_CheckedChanged);
            // 
            // gbBilgisayarBilgisi
            // 
            this.gbBilgisayarBilgisi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gbBilgisayarBilgisi.Controls.Add(this.cbxWebTasarim);
            this.gbBilgisayarBilgisi.Controls.Add(this.cbxVeriTabani);
            this.gbBilgisayarBilgisi.Controls.Add(this.cbxOfis);
            this.gbBilgisayarBilgisi.Controls.Add(this.cbxSistem);
            this.gbBilgisayarBilgisi.Controls.Add(this.cbxYazilim);
            this.gbBilgisayarBilgisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbBilgisayarBilgisi.Location = new System.Drawing.Point(390, 278);
            this.gbBilgisayarBilgisi.Name = "gbBilgisayarBilgisi";
            this.gbBilgisayarBilgisi.Size = new System.Drawing.Size(332, 81);
            this.gbBilgisayarBilgisi.TabIndex = 30;
            this.gbBilgisayarBilgisi.TabStop = false;
            this.gbBilgisayarBilgisi.Text = "Bilgisayar Bilgisi";
            // 
            // cbxWebTasarim
            // 
            this.cbxWebTasarim.AutoSize = true;
            this.cbxWebTasarim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxWebTasarim.Location = new System.Drawing.Point(179, 47);
            this.cbxWebTasarim.Name = "cbxWebTasarim";
            this.cbxWebTasarim.Size = new System.Drawing.Size(109, 20);
            this.cbxWebTasarim.TabIndex = 4;
            this.cbxWebTasarim.Text = "Web Tasarım";
            this.cbxWebTasarim.UseVisualStyleBackColor = true;
            // 
            // cbxVeriTabani
            // 
            this.cbxVeriTabani.AutoSize = true;
            this.cbxVeriTabani.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxVeriTabani.Location = new System.Drawing.Point(54, 47);
            this.cbxVeriTabani.Name = "cbxVeriTabani";
            this.cbxVeriTabani.Size = new System.Drawing.Size(97, 20);
            this.cbxVeriTabani.TabIndex = 3;
            this.cbxVeriTabani.Text = "Veri Tabanı";
            this.cbxVeriTabani.UseVisualStyleBackColor = true;
            // 
            // cbxOfis
            // 
            this.cbxOfis.AutoSize = true;
            this.cbxOfis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxOfis.Location = new System.Drawing.Point(233, 21);
            this.cbxOfis.Name = "cbxOfis";
            this.cbxOfis.Size = new System.Drawing.Size(81, 20);
            this.cbxOfis.TabIndex = 2;
            this.cbxOfis.Text = "MSOffice";
            this.cbxOfis.UseVisualStyleBackColor = true;
            // 
            // cbxSistem
            // 
            this.cbxSistem.AutoSize = true;
            this.cbxSistem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxSistem.Location = new System.Drawing.Point(131, 21);
            this.cbxSistem.Name = "cbxSistem";
            this.cbxSistem.Size = new System.Drawing.Size(68, 20);
            this.cbxSistem.TabIndex = 1;
            this.cbxSistem.Text = "Sistem";
            this.cbxSistem.UseVisualStyleBackColor = true;
            // 
            // cbxYazilim
            // 
            this.cbxYazilim.AutoSize = true;
            this.cbxYazilim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxYazilim.Location = new System.Drawing.Point(13, 21);
            this.cbxYazilim.Name = "cbxYazilim";
            this.cbxYazilim.Size = new System.Drawing.Size(70, 20);
            this.cbxYazilim.TabIndex = 0;
            this.cbxYazilim.Text = "Yazılım";
            this.cbxYazilim.UseVisualStyleBackColor = true;
            // 
            // gbYabanciDil
            // 
            this.gbYabanciDil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gbYabanciDil.Controls.Add(this.cbxArapca);
            this.gbYabanciDil.Controls.Add(this.cbxRusca);
            this.gbYabanciDil.Controls.Add(this.cbxAlmanca);
            this.gbYabanciDil.Controls.Add(this.cbxFransizca);
            this.gbYabanciDil.Controls.Add(this.cbxIngilizce);
            this.gbYabanciDil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbYabanciDil.Location = new System.Drawing.Point(390, 190);
            this.gbYabanciDil.Name = "gbYabanciDil";
            this.gbYabanciDil.Size = new System.Drawing.Size(332, 82);
            this.gbYabanciDil.TabIndex = 29;
            this.gbYabanciDil.TabStop = false;
            this.gbYabanciDil.Text = "Yabancı Dil";
            // 
            // cbxArapca
            // 
            this.cbxArapca.AutoSize = true;
            this.cbxArapca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxArapca.Location = new System.Drawing.Point(189, 49);
            this.cbxArapca.Name = "cbxArapca";
            this.cbxArapca.Size = new System.Drawing.Size(71, 20);
            this.cbxArapca.TabIndex = 4;
            this.cbxArapca.Text = "Arapça";
            this.cbxArapca.UseVisualStyleBackColor = true;
            // 
            // cbxRusca
            // 
            this.cbxRusca.AutoSize = true;
            this.cbxRusca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxRusca.Location = new System.Drawing.Point(85, 49);
            this.cbxRusca.Name = "cbxRusca";
            this.cbxRusca.Size = new System.Drawing.Size(66, 20);
            this.cbxRusca.TabIndex = 3;
            this.cbxRusca.Text = "Rusça";
            this.cbxRusca.UseVisualStyleBackColor = true;
            // 
            // cbxAlmanca
            // 
            this.cbxAlmanca.AutoSize = true;
            this.cbxAlmanca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxAlmanca.Location = new System.Drawing.Point(252, 23);
            this.cbxAlmanca.Name = "cbxAlmanca";
            this.cbxAlmanca.Size = new System.Drawing.Size(80, 20);
            this.cbxAlmanca.TabIndex = 2;
            this.cbxAlmanca.Text = "Almanca";
            this.cbxAlmanca.UseVisualStyleBackColor = true;
            // 
            // cbxFransizca
            // 
            this.cbxFransizca.AutoSize = true;
            this.cbxFransizca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxFransizca.Location = new System.Drawing.Point(125, 23);
            this.cbxFransizca.Name = "cbxFransizca";
            this.cbxFransizca.Size = new System.Drawing.Size(85, 20);
            this.cbxFransizca.TabIndex = 1;
            this.cbxFransizca.Text = "Fransızca";
            this.cbxFransizca.UseVisualStyleBackColor = true;
            // 
            // cbxIngilizce
            // 
            this.cbxIngilizce.AutoSize = true;
            this.cbxIngilizce.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cbxIngilizce.Location = new System.Drawing.Point(13, 23);
            this.cbxIngilizce.Name = "cbxIngilizce";
            this.cbxIngilizce.Size = new System.Drawing.Size(75, 20);
            this.cbxIngilizce.TabIndex = 0;
            this.cbxIngilizce.Text = "İngilizce";
            this.cbxIngilizce.UseVisualStyleBackColor = true;
            // 
            // dtpDTarihi
            // 
            this.dtpDTarihi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dtpDTarihi.Location = new System.Drawing.Point(139, 383);
            this.dtpDTarihi.Name = "dtpDTarihi";
            this.dtpDTarihi.Size = new System.Drawing.Size(216, 22);
            this.dtpDTarihi.TabIndex = 28;
            this.dtpDTarihi.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            // 
            // txtNotlar
            // 
            this.txtNotlar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtNotlar.Location = new System.Drawing.Point(139, 478);
            this.txtNotlar.Multiline = true;
            this.txtNotlar.Name = "txtNotlar";
            this.txtNotlar.Size = new System.Drawing.Size(583, 88);
            this.txtNotlar.TabIndex = 26;
            // 
            // txtAdres
            // 
            this.txtAdres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtAdres.Location = new System.Drawing.Point(138, 257);
            this.txtAdres.Multiline = true;
            this.txtAdres.Name = "txtAdres";
            this.txtAdres.Size = new System.Drawing.Size(217, 102);
            this.txtAdres.TabIndex = 27;
            // 
            // txtTC
            // 
            this.txtTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtTC.Location = new System.Drawing.Point(140, 214);
            this.txtTC.Name = "txtTC";
            this.txtTC.Size = new System.Drawing.Size(215, 22);
            this.txtTC.TabIndex = 25;
            // 
            // txtSoyad
            // 
            this.txtSoyad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSoyad.Location = new System.Drawing.Point(138, 137);
            this.txtSoyad.Name = "txtSoyad";
            this.txtSoyad.Size = new System.Drawing.Size(217, 22);
            this.txtSoyad.TabIndex = 24;
            // 
            // txtAd
            // 
            this.txtAd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtAd.Location = new System.Drawing.Point(138, 107);
            this.txtAd.Name = "txtAd";
            this.txtAd.Size = new System.Drawing.Size(217, 22);
            this.txtAd.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(502, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // lblNotlar
            // 
            this.lblNotlar.AutoSize = true;
            this.lblNotlar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblNotlar.Location = new System.Drawing.Point(75, 481);
            this.lblNotlar.Name = "lblNotlar";
            this.lblNotlar.Size = new System.Drawing.Size(58, 16);
            this.lblNotlar.TabIndex = 20;
            this.lblNotlar.Text = "Notları:";
            // 
            // lblEgitim
            // 
            this.lblEgitim.AutoSize = true;
            this.lblEgitim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblEgitim.Location = new System.Drawing.Point(75, 429);
            this.lblEgitim.Name = "lblEgitim";
            this.lblEgitim.Size = new System.Drawing.Size(55, 16);
            this.lblEgitim.TabIndex = 19;
            this.lblEgitim.Text = "Eğitim:";
            // 
            // lblDTarih
            // 
            this.lblDTarih.AutoSize = true;
            this.lblDTarih.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDTarih.Location = new System.Drawing.Point(28, 388);
            this.lblDTarih.Name = "lblDTarih";
            this.lblDTarih.Size = new System.Drawing.Size(105, 16);
            this.lblDTarih.TabIndex = 18;
            this.lblDTarih.Text = "Doğum Tarihi:";
            // 
            // lblTC
            // 
            this.lblTC.AutoSize = true;
            this.lblTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTC.Location = new System.Drawing.Point(29, 217);
            this.lblTC.Name = "lblTC";
            this.lblTC.Size = new System.Drawing.Size(101, 16);
            this.lblTC.TabIndex = 17;
            this.lblTC.Text = "TC Kimlik No:";
            // 
            // lblAdres
            // 
            this.lblAdres.AutoSize = true;
            this.lblAdres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAdres.Location = new System.Drawing.Point(75, 260);
            this.lblAdres.Name = "lblAdres";
            this.lblAdres.Size = new System.Drawing.Size(53, 16);
            this.lblAdres.TabIndex = 16;
            this.lblAdres.Text = "Adres:";
            // 
            // lblTel
            // 
            this.lblTel.AutoSize = true;
            this.lblTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTel.Location = new System.Drawing.Point(68, 178);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(65, 16);
            this.lblTel.TabIndex = 15;
            this.lblTel.Text = "Telefon:";
            // 
            // lblSoyad
            // 
            this.lblSoyad.AutoSize = true;
            this.lblSoyad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSoyad.Location = new System.Drawing.Point(76, 140);
            this.lblSoyad.Name = "lblSoyad";
            this.lblSoyad.Size = new System.Drawing.Size(57, 16);
            this.lblSoyad.TabIndex = 21;
            this.lblSoyad.Text = "Soyad:";
            // 
            // lblAd
            // 
            this.lblAd.AutoSize = true;
            this.lblAd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAd.Location = new System.Drawing.Point(101, 110);
            this.lblAd.Name = "lblAd";
            this.lblAd.Size = new System.Drawing.Size(31, 16);
            this.lblAd.TabIndex = 14;
            this.lblAd.Text = "Ad:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(774, 661);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.lblBaslik);
            this.Controls.Add(this.btnBasvur);
            this.Controls.Add(this.cbEgitim);
            this.Controls.Add(this.gbAskerlik);
            this.Controls.Add(this.gbCinsiyet);
            this.Controls.Add(this.gbBilgisayarBilgisi);
            this.Controls.Add(this.gbYabanciDil);
            this.Controls.Add(this.dtpDTarihi);
            this.Controls.Add(this.txtNotlar);
            this.Controls.Add(this.txtAdres);
            this.Controls.Add(this.txtTC);
            this.Controls.Add(this.txtSoyad);
            this.Controls.Add(this.txtAd);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblNotlar);
            this.Controls.Add(this.lblEgitim);
            this.Controls.Add(this.lblDTarih);
            this.Controls.Add(this.lblTC);
            this.Controls.Add(this.lblAdres);
            this.Controls.Add(this.lblTel);
            this.Controls.Add(this.lblSoyad);
            this.Controls.Add(this.lblAd);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbAskerlik.ResumeLayout(false);
            this.gbAskerlik.PerformLayout();
            this.gbCinsiyet.ResumeLayout(false);
            this.gbCinsiyet.PerformLayout();
            this.gbBilgisayarBilgisi.ResumeLayout(false);
            this.gbBilgisayarBilgisi.PerformLayout();
            this.gbYabanciDil.ResumeLayout(false);
            this.gbYabanciDil.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txtTelefon;
        private System.Windows.Forms.Label lblBaslik;
        private System.Windows.Forms.Button btnBasvur;
        private System.Windows.Forms.ComboBox cbEgitim;
        private System.Windows.Forms.GroupBox gbAskerlik;
        private System.Windows.Forms.RadioButton rbYapilmadi;
        private System.Windows.Forms.RadioButton rbTecilli;
        private System.Windows.Forms.RadioButton rbYapildi;
        private System.Windows.Forms.GroupBox gbCinsiyet;
        private System.Windows.Forms.RadioButton rbKadin;
        private System.Windows.Forms.RadioButton rbErkek;
        private System.Windows.Forms.GroupBox gbBilgisayarBilgisi;
        private System.Windows.Forms.CheckBox cbxWebTasarim;
        private System.Windows.Forms.CheckBox cbxVeriTabani;
        private System.Windows.Forms.CheckBox cbxOfis;
        private System.Windows.Forms.CheckBox cbxSistem;
        private System.Windows.Forms.CheckBox cbxYazilim;
        private System.Windows.Forms.GroupBox gbYabanciDil;
        private System.Windows.Forms.CheckBox cbxArapca;
        private System.Windows.Forms.CheckBox cbxRusca;
        private System.Windows.Forms.CheckBox cbxAlmanca;
        private System.Windows.Forms.CheckBox cbxFransizca;
        private System.Windows.Forms.CheckBox cbxIngilizce;
        private System.Windows.Forms.DateTimePicker dtpDTarihi;
        private System.Windows.Forms.TextBox txtNotlar;
        private System.Windows.Forms.TextBox txtAdres;
        private System.Windows.Forms.TextBox txtTC;
        private System.Windows.Forms.TextBox txtSoyad;
        private System.Windows.Forms.TextBox txtAd;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblNotlar;
        private System.Windows.Forms.Label lblEgitim;
        private System.Windows.Forms.Label lblDTarih;
        private System.Windows.Forms.Label lblTC;
        private System.Windows.Forms.Label lblAdres;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblSoyad;
        private System.Windows.Forms.Label lblAd;
    }
}

